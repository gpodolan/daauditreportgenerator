Import-Module AzureAD
Import-Module ExchangeOnlineManagement

foreach($i in (Get-Content .\conn.conf)){
    Set-Variable -Name $i.split("=")[0] -Value $i.split("=",2)[1]
}

Connect-AzureAD -ApplicationId $appId -TenantId $tenantId -CertificateThumbprint $certThumbprint

$GroupMembers = Get-AzureADGroupMember -ObjectId $groupObjId

$members=""
foreach ($member in $GroupMembers) {
    $members = $members + """" + $member.UserPrincipalName + """, "
}

$ssAc=ConvertTo-SecureString $ac

$creds=New-Object System.Management.Automation.PSCredential ($id, $ssAc)

Connect-IPPSSession -Credential $creds

Set-UnifiedAuditLogRetentionPolicy -Identity "DomainAdminRetention" -Priority 10 -RetentionDuration SixMonths -UserIds $members