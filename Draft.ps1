Import-Module AzureAD
Import-Module ExchangePowerShell
Import-Module ExchangeOnlineManagement
Import-Module Az.Accounts
Import-Module Az.Storage
Import-Module Az.Resources

#Update retention policy
Connect-AzureAD -ApplicationId b0ae44fd-0398-478f-b657-ec6cac42ed86 `
 -CertificateThumbprint 7F96E5DEF182704927965EED5A8366F6D7B129D1 -TenantId 97386f4d-e17a-46a3-ab4b-cb9da3a17243
$GroupMembers = Get-AzureADGroupMember -ObjectId 3d05d2ec-5c96-432b-a200-bdc607712e89
$members=""
foreach ($member in $GroupMembers) {
    $members = $members + """" + $member.UserPrincipalName + """, "
}
$members

#New-UnifiedAuditLogRetentionPolicy -Name "DomainAdminRetention" -Description "Retention policy for domain admin activity" -RetentionDuration SixMonths -Priority 10 -UserIds $members
Set-UnifiedAuditLogRetentionPolicy -Identity "DomainAdminRetention" -Priority 10 -RetentionDuration SixMonths -UserIds $members

#Search audit logs
Set-ExecutionPolicy RemoteSigned
Connect-ExchangeOnline -CertificateThumbprint 7F96E5DEF182704927965EED5A8366F6D7B129D1 -AppId b0ae44fd-0398-478f-b657-ec6cac42ed86 -Organization "ariat.com" -ShowBanner $false

$startDate=[DateTime]::UtcNow.AddDays(-90)
$endDate=[DateTime]::UtcNow
$outFileName=$startDate.ToUniversalTime("yyyy-MM-dd").ToString() + "--" + $endDate.ToUniversalTime("yyyy-MM-dd").ToString() + ".csv"

$content="Creation Date | Record Type | User IDs | Operations | Is Valid | Object State `r`n Audit Data"
$reportItems=Search-UnifiedAuditLog -UserIds $members -StartDate $startDate -EndDate $endDate

foreach ($item in $reportItems) {
    $line=$item.CreationDate.ToString("ddd, dd MMM yyyy HH:mm:ss UTC") + " | " + $item.RecordType + " | " + $item.UserIds + " | " + $item.Operations + " | "  + $item.IsValid + " | " + $item.ObjectState + " `r`n " + $item.AuditData
    $content=$content + $line
}

$content | Out-File -FilePath $outFileName

Connect-AzAccount

